require("@babel/polyfill");

const fs = require("fs");
const browserify = require("browserify");
const watchify = require("watchify");
const tsify = require("tsify");
const babelify = require("babelify");

const bundler = browserify({
    entries: ["source/Main.ts"],
    cache: {},
    packageCache: {}
})
    .plugin(watchify)
    .plugin(tsify)
    .transform(babelify, {
        presets: ["@babel/preset-env"],
        plugins: ["@babel/plugin-transform-runtime"],
        extensions: [".ts"]
    });

bundler.on("update", bundle);
bundler.on("log", console.log);

function bundle() {
    bundler.bundle(function (err, buf) {
        if (err) console.error(err);
        fs.writeFileSync("public/bundle.js", buf);
    });
}

bundle();
