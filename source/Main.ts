import {
    Engine,
    Entity,
    Renderable,
    CanvasRenderingSystem,
    Scene,
    System,
    Rectangle,
    Transform,
    Polygon,
    MouseInput,
    Component,
    MouseInputSystem,
    Arc
} from "../engine/source/PrimeEngine";

export class InputSystem extends System {
    public readonly queries = { "": [MouseInput] };

    private previous: MouseInput = new MouseInput();
    private engine: Engine;

    private points: Entity[] = [];
    private selected: Entity | undefined;

    constructor(engine: Engine) {
        super();

        this.engine = engine;
    }

    public start(query: string, entities: Entity[]): void {
        if (this.engine.scene) {
            const polygon = new Entity("polygon", [
                Polygon,
                Renderable,
                Transform
            ]);
            polygon.components.get(Renderable).fill.enabled = true;

            this.engine.scene.entities.add(polygon);
        }
    }

    public preUpdate(entities: Entity[], delta: number): void {
        const component = entities[0].components.get(MouseInput);

        if (!this.engine.scene) return;

        let on = undefined;

        for (const point of this.points) {
            const transform = point.components.get(Transform);
            const radius = point.components.get(Arc).radius;
            if (
                (component.position.x - transform.position.x) ** 2 +
                    (component.position.y - transform.position.y) ** 2 <
                radius ** 2
            ) {
                on = point;
            }
        }

        if (component.left && !this.previous.left && !this.selected && on) {
            this.selected = on;
            this.selected.components.get(Renderable).fill.style = "red";
        } else if (
            component.left &&
            !this.previous.left &&
            !this.selected &&
            !on
        ) {
            const point = new Entity(this.points.length.toString(), [
                Arc,
                Renderable,
                Transform
            ]);
            point.components.get(Arc).radius = 5;
            point.components.get(Transform).position = {
                ...component.position
            };
            point.components.get(Renderable).fill.enabled = true;
            point.components.get(Renderable).stroke.enabled = true;
            point.components.get(Renderable).stroke.lineWidth = 2;
            point.components.get(Renderable).stroke.style = "gray";

            this.points.push(point);
            this.engine.scene.entities.add(point);
        } else if (!component.left && this.previous.left && this.selected) {
            this.selected.components.get(Renderable).fill.style = "black";
            this.selected = undefined;
        } else if (this.selected) {
            this.selected.components.get(Transform).position = {
                ...component.position
            };
        } else if (
            component.right &&
            !this.previous.right &&
            on &&
            !this.selected &&
            this.points[parseInt(on.name)]
        ) {
            this.points.splice(parseInt(on.name));
            this.engine.scene.entities.remove(on.name);
        }

        const vertices = [];

        for (const point of this.points) {
            vertices.push(point.components.get(Transform).position);
        }

        this.engine.scene.entities
            .get("polygon")!
            .components.get(Polygon).vertices = vertices;

        this.previous = { ...component };
    }

    public update(query: string, entities: Entity[], delta: number): void {}
}

async function main(): Promise<void> {
    const canvas = document.getElementById("PrimeEngine") as HTMLCanvasElement;

    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    const engine = new Engine({});

    const input = new Entity("input", [MouseInput]);

    const scene = new Scene(
        "main",
        [input],
        [
            new CanvasRenderingSystem(canvas),
            new MouseInputSystem(canvas),
            new InputSystem(engine)
        ]
    );

    engine.scene = scene;
    engine.start();
}

window.onload = main;
